# ----------------------------- Corrélation ------------------------------------
rm(list = ls())
load("0. Ressources/Discours macron/Macron_discours.RData")

source("2. Packages/Vizualisation/Mise en place.R", encoding="UTF-8")

#rownames(correlation_rc)<- paste0(rownames(correlation_rc), ".x")

#correlation_rc[3,4]<-0
data_cor<-mots_frequents_evol%>%
  select(contains("count"))%>%
  cor(method="spearman")%>%
  data.frame()

data_cor$line <- rownames(data_cor)
data_cor_rc <- data_cor%>%
  reshape2::melt()%>%
  rename(var2=variable, var1=line, var3=value)

ggplot(data = data_cor_rc,
       aes(x = var1, y = var2,
           fill=var3, label = round(var3, 2))) +
  VizPlot_Table()+
  VizPlot_Table(method="text", diag=FALSE, size=5) +
  
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 45, hjust = 1))+
  VizTools_Legend()
