# --------------------------- Aesthetics ---------------------------------------

# 
# Mat <- function(line=NULL, col=NULL, value=NULL) {
#   # Convertir les arguments en expressions
#   line_expr <- substitute(line)
#   col_expr <- substitute(col)
#   value_expr <- substitute(value)
# 
#   # Créer une liste contenant les expressions
#   mat_list <- list(line = line_expr, col = col_expr, value = value_expr)
# 
#   # Ajouter une classe "mat" à la liste
#   class(mat_list) <- "Mat"
# 
#   # Retourner la liste avec la classe "mat"
#   return(mat_list)
# }


Var <- function(x=NULL, y=NULL, color=NULL, fill=NULL, size=NULL, alpha=NULL, shape=NULL
                #, text=NULL
                ) {
  # Convertir les arguments en expressions
  x_expr <- substitute(x)
  y_expr <- substitute(y)
  color_expr <- substitute(color)
  fill_expr <- substitute(fill)
  size_expr <- substitute(size)
  alpha_expr <- substitute(alpha)
  shape_expr <- substitute(shape)
#  text_expr <- substitute(text)
  
  # Créer une liste contenant les expressions
  var_list <- list(x = x_expr, y = y_expr, color = color_expr,
                   fill = fill_expr, size = size_expr, alpha = alpha_expr,
                   shape=shape_expr#,
                   #text=text_expr
                   )
  
  # Ajouter une classe "mat" à la liste
  class(var_list) <- "Var"
  
  # Retourner la liste avec la classe "mat"
  return(var_list)
}


# mat_to_Var <- function(mat_obj) {
#   # Vérifier que l'objet d'entrée est de classe "mat"
#   if (!inherits(mat_obj, "Mat")) {
#     stop("L'objet d'entrée doit être de classe 'mat'.")
#   }
#   # Fonction pour extraire le nom de l'argument
#   line_arg <- Var(x=ifelse(any(str_detect(paste(mat_obj$line), "x")), substitute(x), NULL),
#                   y=ifelse(any(str_detect(paste(mat_obj$line), "y")), substitute(y), NULL),
#                   color=ifelse(any(str_detect(paste(mat_obj$line), "color")), substitute(color), NULL),
#                   fill=ifelse(any(str_detect(paste(mat_obj$line), "fill")), substitute(fill), NULL),
#                   size=ifelse(any(str_detect(paste(mat_obj$line), "size")), substitute(size), NULL),
#                   alpha=ifelse(any(str_detect(paste(mat_obj$line), "alpha")), substitute(alpha), NULL),
#                   shape=ifelse(any(str_detect(paste(mat_obj$line), "shape")), substitute(shape), NULL),
#                   text=ifelse(any(str_detect(paste(mat_obj$line), "text")), substitute(text), NULL))
#   
#   col_arg <- Var(x=ifelse(any(str_detect(paste(mat_obj$col), "x")), substitute(x), NULL),
#                  y=ifelse(any(str_detect(paste(mat_obj$col), "y")), substitute(y), NULL),
#                  color=ifelse(any(str_detect(paste(mat_obj$col), "color")), substitute(color), NULL),
#                  fill=ifelse(any(str_detect(paste(mat_obj$col), "fill")), substitute(fill), NULL),
#                  size=ifelse(any(str_detect(paste(mat_obj$col), "size")), substitute(size), NULL),
#                  alpha=ifelse(any(str_detect(paste(mat_obj$col), "alpha")), substitute(alpha), NULL),
#                  shape=ifelse(any(str_detect(paste(mat_obj$col), "shape")), substitute(shape), NULL),
#                  text=ifelse(any(str_detect(paste(mat_obj$col), "text")), substitute(text), NULL))
#   
#   value_arg <- Var(x=ifelse(any(str_detect(paste(mat_obj$value), "x")), substitute(x), NULL),
#                    y=ifelse(any(str_detect(paste(mat_obj$value), "y")), substitute(y), NULL),
#                    color=ifelse(any(str_detect(paste(mat_obj$value), "color")), substitute(color), NULL),
#                    fill=ifelse(any(str_detect(paste(mat_obj$value), "fill")), substitute(fill), NULL),
#                    size=ifelse(any(str_detect(paste(mat_obj$value), "size")), substitute(size), NULL),
#                    alpha=ifelse(any(str_detect(paste(mat_obj$value), "alpha")), substitute(alpha), NULL),
#                    shape=ifelse(any(str_detect(paste(mat_obj$value), "shape")), substitute(shape), NULL),
#                    text=ifelse(any(str_detect(paste(mat_obj$value), "text")), substitute(text), NULL))
#   
#   
#   # Créer un objet de classe Var à partir de la liste
#   var_obj <- Var()
#   
#   var_obj$x <- case_when(
#     any(str_detect(paste(mat_obj$line), "x")) ~ "line",
#     any(str_detect(paste(mat_obj$col), "x")) ~ "col",
#     any(str_detect(paste(mat_obj$value), "x"))~ "value"
#   )
#   
#   var_obj$y <- case_when(
#     any(str_detect(paste(mat_obj$line), "y")) ~ "line",
#     any(str_detect(paste(mat_obj$col), "y")) ~ "col",
#     any(str_detect(paste(mat_obj$value), "y"))~ "value"
#     
#   )
#   
#   var_obj$color <- case_when(
#     any(str_detect(paste(mat_obj$line), "color")) ~ "line",
#     any(str_detect(paste(mat_obj$col), "color")) ~ "col",
#     any(str_detect(paste(mat_obj$value), "color"))~ "value"
#   )
#   
#   var_obj$fill <- case_when(
#     any(str_detect(paste(mat_obj$line), "fill")) ~ "line",
#     any(str_detect(paste(mat_obj$col), "fill")) ~ "col",
#     any(str_detect(paste(mat_obj$value), "fill"))~ "value"
#   )
#   
#   var_obj$size <- case_when(
#     any(str_detect(paste(mat_obj$line), "size")) ~ "line",
#     any(str_detect(paste(mat_obj$col), "size")) ~ "col",
#     any(str_detect(paste(mat_obj$value), "size"))~ "value"
#   )
#   
#   var_obj$alpha <- case_when(
#     any(str_detect(paste(mat_obj$line), "alpha")) ~ "line",
#     any(str_detect(paste(mat_obj$col), "alpha")) ~ "col",
#     any(str_detect(paste(mat_obj$value), "alpha"))~ "value"
#   )
#   
#   var_obj$shape <- case_when(
#     any(str_detect(paste(mat_obj$line), "shape")) ~ "line",
#     any(str_detect(paste(mat_obj$col), "shape")) ~ "col",
#     any(str_detect(paste(mat_obj$value), "shape"))~ "value"
#   )
#   
#   
#   var_obj$text <- case_when(
#     any(str_detect(paste(mat_obj$line), "text")) ~ "line",
#     any(str_detect(paste(mat_obj$col), "text")) ~ "col",
#     any(str_detect(paste(mat_obj$value), "text"))~ "value"
#   )
#   
#   
#   
#   
#   # Retourner l'objet de classe Var
#   
#   return(var_obj)
# }




# ------------------------- Corrplot -------------------------------------------

VizPlot_Corr<- function(data,
                        mapping=NULL,
                        text = "same",
                        dec=2,
                        type = "full",
                        diag.col = TRUE,
                        diag.text = FALSE,
                        reverse=FALSE,
                        couleur.text = "auto",
                        #                         couleur.type = c(low = "#8f2d56", mid = "white",  high = "#001f3d", minpoint=-1,maxpoint=1,  midpoint=0, by="auto"),
                        size.text = 5,
                        #                         bar.height=20,
                        legend=TRUE){
  
  #'  @param forme indique la forme que prend la base de donnée insérée : soit "corr" qui signifie qu'il s'agit d'une matrice de corrélation, soit "long" qui signifie qu'il s'agit d'une base de données avec 3 colonnes, la première et la deuxième indiquant le nom des deux modalités mesurées, et la troisième avec la valeur de la corrélation
  #'  @param text précise si la valeur des coefficients de corrélation doit être affichée sur les cases coloriées du graphique : si TRUE, affiche sur toutes les cases, si FALSE sur aucune, si "same" les coefficients apparaissent sur toutes celles qui sont coloriées par des couleurs, et inversement sur toutes celles qui ne le sont pas avec "same" ; enfin, "only" signifie que seul du text est affiché en fonction du type renseignée
  #'  @param dec spécifie le nombre de décimales affichées après. Si text=FALSE, par défaut, aucun texte n'apparaît
  #'  @param type précise la forme du corrplot : "full" pour que toutes les cases soient affichées, sinon seule la moitié est affichée, la moitié basse avec "lower", et haute avec "upper" ;
  #'  @param diag.col pour préciser si la couleur de la diagonale doit être affichée
  #'  @param diag.text pour préciser si la valeur de la diagonale doit être affichée
  #'  @param reverse pour savoir quels axes inverser : par défaut, 0 donc FALSE ; ou "x" pour inverser seulement l'axe des abscisses (respectivement "y") ; TRUE pour inverser les deux
  
  library(reshape2)
  
  #  data <- Lg.group_rc
  
  
  #map=NULL
  # text = "same"
  # dec=2
  # type = "full"
  # diag.col = TRUE
  # diag.text = FALSE
  # reverse=FALSE
  # couleur.text = "auto"
  # #                         couleur.type = c(low = "#8f2d56", mid = "white",  high = "#001f3d", minpoint=-1,maxpoint=1,  midpoint=0, by="auto"),
  # size.text = 5
  
  ### format de la base de données
  #  reverse <- FALSE
  #  map <- Mat(line=x&alpha, col=size&y)
  
  
  if (class(mapping)=="Mat"){
    #    data <- correlation_rc
    data_rc <- data
    
    #    dec=0.2
    
    if(dec != FALSE & is.numeric(dec)==TRUE){
      
      data_rc <- fortify(round(data_rc,  dec))
      
    }
    
    data_rc$line <- rownames(data_rc)
    data_rc <- reshape2::melt(data_rc)%>%
      rename(line=variable, col=line)
    
    map.rc <- mat_to_Var(map)
    
  }else if(class(mapping)!="Var"){
    stop("Erreur dans la base de donnée rentrée, elle ne correspond pas au format indiqué dans le paramètre 'forme'")
    
  }else{
    
    # forme longitudinale à 3 colonnes
    if(length(data) < 3){
      print(length(data))
      stop("Erreur dans le format de la base de donnée rentrée, les 2 premières précisent le nom des varaibles comparées, et la dernière la valeur de son coefficient de corrélation")
      
    }
    
    
    
    
    map.rc$x <- paste0(map.rc$x)
    map.rc$y <- paste0(map.rc$y)
    map.rc$color <- paste0(map.rc$color)
    map.rc$fill <- paste0(map.rc$fill)
    map.rc$size <- paste0(map.rc$size)
    map.rc$alpha <- paste0(map.rc$alpha)
#    map.rc$text <- paste0(map.rc$text)
    map.rc$shape <- paste0(map.rc$shape)
    
    
    #    data_rc <- data_cor_rc%>%
    data_rc <- data%>%
      rename(col=map.rc$x,
             line=map.rc$y,
             value=map.rc$fill)
    
    map.rc$x <- "col"
    map.rc$y <- "line"
    
    
    
    #    colnames(data_rc) <- c( "line", "col","value")
    
    
    print(map.rc)
    
    #dec =0.2
    
    
    if(dec != FALSE & is.numeric(dec)==TRUE){
      
      data_rc <- data_rc %>%
        mutate(value = round(value,dec) )
      
    }
    print(map.rc)
    
  }
  
  print(map.rc)
  
  
  if(is.na(map.rc$y)==TRUE | ! map.rc$y %in% c("col", "line") | is.na(map.rc$x)==TRUE | ! map.rc$x %in% c("col", "line")){
    print(map.rc$x)
    print(map.rc$y)
    stop("map=Mat() n'a pas été renseigné correctement : il faut renseigné correctement, il faut que x et y soient renseignés dans 'col' et 'line', et que x et y soient différents")
  }
  
  # if(map.rc$y=="line" & map.rc$x == "col" ){
  #   print(data_rc)
  #   # data_rc <- data_rc %>%
  #   #   rename(position_i=position_j, position_j=position_i)
  #   
  #   map.rc$y<-"col"
  #   map.rc$x<-"line"
  #   
  #   print(data_rc)
  #   
  # }
  
  ### type
  
  if(type=="full"){
    lower <- TRUE
    upper <- TRUE
    
  }
  
  if(type=="upper"){
    lower <- FALSE
    upper <-  TRUE
    
  }
  
  if(type=="lower"){
    lower <- TRUE
    upper <- FALSE
    
  }
  
  ### sélection des paramètres d'affichage du texte
  if(!text %in% c("inverse", TRUE,FALSE, "same", "only")){
    print("Avis - text mal renseigné, 3 valeurs possibles : TRUE, FALSE, inverse")
    print("------> Par défaut, nous avons laissé text = 'inverse'")
    text <- "inverse"
    
    
  }
  
  if(text=="inverse"){
    
    text.lower<-!lower
    text.upper<-!upper
    
  }else if(text==TRUE){
    
    text.lower<-TRUE
    text.upper<-TRUE
    
  }else if(text=="same"){
    
    text.lower<-lower
    text.upper<-upper
    
  }else if(text=="only"){
    
    text.lower<-lower
    text.upper<-upper
    lower <- FALSE
    upper <- FALSE
    diag.col <- FALSE
    
  }else if(text==FALSE){
    
    text.lower<-FALSE
    text.upper<-FALSE
  }else{
    stop("Il y a une erreur inconnue dans les paramètres rentrés dans 'text'")
    
  }
  
  
  
  ### Gestion de la forme particulière d'une matrice
  n <- as.numeric(dplyr::count(data_rc))
  if(sqrt(n)%%1==1){
    
    
    print("Note : matrice irrégulière")
    
  }
  
  
  ### Mise en forme de la base de données du graphique, prenant en compte les paramétrages
  data_rc <- data_rc %>%
    mutate(id = 1:n) %>%
    mutate(position_j=rep(1:sqrt(n), sqrt(n)))%>%
    mutate(position_i=as.numeric(gl(sqrt(n), sqrt(n))))
  
  #  print("y")
  if(map.rc$y=="col" & map.rc$x == "line" ){
    
    #    print("y")
    ordre_y<- data_rc%>%
      select(position_j, col) %>%
      distinct() %>%
      arrange(rev(position_j)) %>%
      select(col)
    
    ordre_x<- data_rc%>%
      select(position_i, line) %>%
      distinct() %>%
      arrange(position_i) %>%
      select(line)
    
    #  print("y")
    if(reverse==TRUE | reverse == "y"){
      data_rc <- data_rc %>%
        rename(position_j=position_i, position_i=position_j)
      
      ordre_y$col <- rev(ordre_y$col)
      
    }
    ordre_y2 <- ordre_y$col
    
    
    
    if(reverse==TRUE | reverse == "x"){
      
      ordre_x$line <- rev(ordre_x$line)
      
    }
    ordre_x2<- ordre_x$line
    
    
    
  }else if(map.rc$y=="line" & map.rc$x == "col"){
    
    # data_rc <- data_rc %>%
    #  rename(line=col, col=line)
    
    #  print("y")
    ordre_y<- data_rc%>%
      select(position_i, line) %>%
      distinct() %>%
      arrange(rev(position_i)) %>%
      select(line)
    
    ordre_x<- data_rc%>%
      select(position_j, col) %>%
      distinct() %>%
      arrange(rev(position_j)) %>%
      select(col)
    
    #      print(ordre_y)
    
    #      print("okay")
    #      ordre_x$col <- rev(ordre_x$col)
    #      ordre_y$line <- rev(ordre_y$line)
    
    #      print(paste(lower, upper, text.lower, text.upper))
    
    if(type%in%c("lower", "upper")){
      lower <- !lower
      upper<- !upper
      text.lower<-!text.lower
      text.upper<-!text.upper
      
      
    }
    
    
    #      print("okay")
    if(reverse==TRUE | reverse == "y"){
      # data_rc <- data_rc %>%
      #   rename(position_j=position_i, position_i=position_j)
      
      #        print("y")
      ordre_y$line <- rev(ordre_y$line)
      
      
      if(type != "full"){
        lower <- !lower
        upper<- !upper
        text.lower<-!text.lower
        text.upper<-!text.upper
      }
      
      #         print(ordre_y)
      
    }
    ordre_y2<- ordre_y$line
    
    
    
    
    if(reverse==TRUE | reverse == "x"){
      
      #        print("x")
      
      ordre_x$col <- rev(ordre_x$col)
      
      #        print(ordre_x)
      
    }
    ordre_x2<- rev(ordre_x$col)
    
    
    
    #      print("okay")
    
  }
  
  
  
  
  
  # if(map.rc$y=="line" & map.rc$x == "col" ){
  # 
  #   data_rc <- data_rc %>%
  #     rename(position_j=position_i, position_i=position_j)
  # 
  # }
  
  
  
  
  data_rc <- data_rc %>%
    
    # mutate(lower.var=lower,
    #        upper.var= upper,
    #        text.lower.var=text.lower,
    #        text.upper.var=text.upper,
    #        diag.col.var=diag.col,
    #        diag.text.var=diag.text
    #        ) %>%
    mutate(affichage.value = case_when(
      position_i>position_j& upper==TRUE~ value,
      position_i<position_j & lower==TRUE~ value,
      position_i==position_j & diag.col==TRUE~ value,
    )) %>%
    
    # mutate(value2 = case_when(
    #   as.numeric(line)>as.numeric(col) & upper.var==TRUE ~ value,
    #   as.numeric(line)<as.numeric(col) & lower.var==TRUE ~ value,
    #   as.numeric(line)==as.numeric(col) & diag.col.var==TRUE~ value)) %>%
    
    mutate(affichage.text = case_when(
      position_i>position_j ~ text.upper,
      position_i<position_j ~ text.lower,
      position_i==position_j ~ diag.text)) %>%
    
    mutate(affichage.case_couleur = ifelse(is.na(affichage.value)==TRUE, FALSE, TRUE)) %>%
    
    mutate(affichage.value = case_when(
      affichage.case_couleur==FALSE & affichage.text==TRUE ~ 0,
      TRUE ~ affichage.value
    )) %>%
    
    mutate(color.text = case_when(
      couleur.text=="auto" & affichage.text==TRUE & affichage.case_couleur==FALSE ~ "black",
      couleur.text=="auto" & affichage.text==TRUE & affichage.case_couleur==TRUE ~ "white",
      tryCatch(!is.null(col2rgb(couleur.text)), error = function(e) FALSE) ~ couleur.text,
      TRUE ~ "red"
      
    ))
  
  
  
  print(data_rc)
  
  
  
  
  graphique <- ggplot(data_rc,  aes_string(x=map.rc$x, y=map.rc$y,
                                           fill = "affichage.value"))+
    geom_tile() +
    coord_fixed(ratio = 1)+
    VizTools_Theme()+
    VizTools_Legend(
      type="couleur",
      format="continuous",
      couleurs = c("low" = "#8f2d56", "mid" = "white", "high" = "#001f3d"),
      limits =c(min=-1,max=1,  0,by='auto'))+
    
    
    geom_text(aes(label = ifelse(affichage.text==TRUE,value, ""),color=color.text),
              vjust = 0.5, size=size.text) +
    
    
    theme(legend.title=element_markdown(size=15),
          plot.caption = element_markdown(size=10),
          plot.title = element_markdown(size=25#, hjust=0.55
          ),
          legend.title.align = 0,
          axis.text.x = element_markdown(angle=45, hjust=1),
          plot.subtitle = element_markdown(size=20#, hjust=-0.5
          ),
          
          axis.text = element_markdown(size=15))+
    labs(
      title = "**Degré d'association**",
      fill="**Coefficient<br>**",
      subtitle = "*Coefficient de corrélation<br>*",
      x = NULL,
      y = NULL,
    )+
    scale_y_discrete(limits=ordre_y2)+
    scale_x_discrete(limits=ordre_x2)+
    
    # scale_y_discrete(limits=ordre_y$col)+
    # scale_x_discrete(limits=ordre_x$line)+
    scale_color_identity()
  
  
  
  if(text == "only"){
    graphique<- graphique +
      theme(
        panel.grid.major = element_line(color="transparent"))
    
  }
  
  
  
  graphique
  
  
  
  
}

